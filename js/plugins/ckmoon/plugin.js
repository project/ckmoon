(function ckmoonPlugin($, Drupal, CKEDITOR) {
  CKEDITOR.plugins.add("ckmoon", {
    icons: "ckmoon",
    init(editor) {
      function insertContent(html) {
        const element = CKEDITOR.dom.element.createFromHtml(html);
        editor.insertElement(element);
      }

      function saveCallback(returnValue) {
        let prefix = "";

        if (typeof editor.config.ckmoon_prefix !== "undefined") {
          prefix = editor.config.ckmoon_prefix;
        }

        const cssClass = prefix + returnValue;

        const content = `<i class="${cssClass}">&thinsp;</i>`;
        insertContent(content);
      }

      // Implementation before initializing plugin.
      editor.addCommand("addCkMoonIcon", {
        canUndo: true,
        exec(editor) {
          // Open dialog form.
          Drupal.ckeditor.openDialog(
            editor,
            Drupal.url("ckmoon/form/ckmoon_editor_dialog"),
            {},
            saveCallback,
            {},
          );
        },
      });

      editor.ui.addButton("CkMoon", {
        label: Drupal.t("CK Moon"),
        command: "addCkMoonIcon",
      });
    },
  });
})(jQuery, Drupal, CKEDITOR);

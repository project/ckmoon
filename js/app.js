(function ckmoonApp($, Drupal) {
  /**
   * Behaviors for ckmoon dialog popup.
   */
  Drupal.behaviors.ckmoon = {
    attach(context) {
      $(once("ckmoon", ".ckmoon__icon-fieldset", context)).each(
        function ckmoonAppEach() {
          const $icon = $(".ckmoon__icon", $(this));
          $icon.click(function ckmoonAppClick() {
            $(this).next(".ckmoon__icon-button").trigger("click");
          });
        },
      );
    },
  };
})(jQuery, Drupal);

(function ckMoon($, window, Drupal) {
  Drupal.AjaxCommands.prototype.CkmoonSelection = function ckMoonAjax(
    ajax,
    response,
  ) {
    const $valueInput = $(".ckmoon-value", response.selector);
    $valueInput.val(response.iconCode);
    $valueInput.trigger("keyup");
    $(".ckmoon-preview", response.selector).html(response.svg);
    $(".ckmoon-preview-title", response.selector).html(response.iconCode);
  };
})(jQuery, window, Drupal);

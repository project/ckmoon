CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

CK Moon provides the ability to embed icomoon fonts defined in your theme
into the wysiwyg.

The plugin will read the selection.json file provided by your icomoon pack and display the icons in a handy dialog for you to choose from.
An `i` tag will then be inserted into the wysiwyg with the relevent icon classname. eg: `<i class="icon-arrow-right"></i>`.


REQUIREMENTS
------------

* CKEditor module.
* Icomoon font, along with its generated selection.json file.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

* Drag the ckmoon button into the toolbar when configuring your text formater.
* If "Limit allowed HTML tags" is enabled, ensure the `<i>` tag is in the list of
Allowed HTML tags.
* Configure the CKEditor plugin settings.
  * Add a path to the icomoon selection.json file, relative to your default theme directory.
  * Add any prefix you may have added to your icon classes.
* In order for the icons to show in the wysiwyg, you must first include your theme style in the `ckeditor_stylesheet` settings in your themes `info.yml` file:
```yaml
    ckeditor_stylesheets:
      - styles/css/main.css
 ```

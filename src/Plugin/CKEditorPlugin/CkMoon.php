<?php

namespace Drupal\ckmoon\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\ckmoon\CkmoonManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\editor\Entity\Editor;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the "ckmoon" plugin.
 *
 * @CKEditorPlugin(
 *   id = "ckmoon",
 *   label = @Translation("CK Moon"),
 *   module = "ckmoon"
 * )
 */
class CkMoon extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface, ContainerFactoryPluginInterface {

  /**
   * Defines a District Icon manager class.
   *
   * @var \Drupal\ckmoon\CkmoonManager
   */
  private $ckmoonManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CkmoonManager $ckmoon_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->ckmoonManager = $ckmoon_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('ckmoon.manager')
    );
  }

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::isInternal().
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return \Drupal::service('extension.list.module')->getPath('ckmoon') . '/js/plugins/ckmoon/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'core/drupal.ajax',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $prefix = $this->ckmoonManager->getIconClassPrefix();
    $config = [];

    if ($prefix) {
      $config['ckmoon_prefix'] = $prefix;
    }

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'CkMoon' => [
        'label' => $this->t('Ckmoon'),
        'image' => \Drupal::service('extension.list.module')->getPath('ckmoon') . '/js/plugins/ckmoon/icons/ckmoon.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
    $form['ckmoon'] = [
      '#type' => 'container',
      'notice' => [
        '#markup' => 'CKMoon settings are now located <a href="/admin/config/ckmoon/settings">here.</a>',
      ],
    ];

    return $form;
  }

}

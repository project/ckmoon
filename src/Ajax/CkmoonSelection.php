<?php

namespace Drupal\ckmoon\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Defines an AJAX command that replaces ckmoon select field markup.
 *
 * @ingroup ajax
 */
class CkmoonSelection implements CommandInterface {

  /**
   * A CSS selector string.
   *
   * @var string
   */
  protected $selector;

  /**
   * SVG markup generated from icomoon json file.
   *
   * @var string
   */
  protected $svg;

  /**
   * Icomoon icon code.
   *
   * @var string
   */
  protected $iconCode;

  /**
   * Constructs a CkmoonSelection object.
   *
   * @param string $selector
   *   The css selector for replacing content.
   * @param string $svg
   *   Selected icon as SVG markup.
   * @param string $iconCode
   *   Selected icon code.
   */
  public function __construct($selector, $svg, $iconCode) {
    $this->selector = $selector;
    $this->svg = $svg;
    $this->iconCode = $iconCode;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'CkmoonSelection',
      'selector' => $this->selector,
      'svg' => $this->svg,
      'iconCode' => $this->iconCode,
    ];
  }

}

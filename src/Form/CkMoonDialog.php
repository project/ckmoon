<?php

namespace Drupal\ckmoon\Form;

use Drupal\ckmoon\Ajax\CkmoonSelection;
use Drupal\ckmoon\CkmoonManager;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a CKMoon Dialog form.
 */
class CkMoonDialog extends FormBase {

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Defines a District Icon manager class.
   *
   * @var \Drupal\ckmoon\CkmoonManager
   */
  private $ckmoonManager;

  /**
   * Turns a render array into a HTML string.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  private $renderer;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Defines the configuration object factory.
   * @param \Drupal\ckmoon\CkmoonManager $ckmoon_manager
   *   Defines a District Icon manager class.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   Turns a render array into a HTML string.
   */
  public function __construct(ConfigFactory $configFactory, CkmoonManager $ckmoon_manager, Renderer $renderer) {
    $this->configFactory = $configFactory;
    $this->ckmoonManager = $ckmoon_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('ckmoon.manager'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ckmoon_dialog';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $input_id = '') {
    $jsonParsed = $this->ckmoonManager->getIconList(TRUE);

    if (empty($jsonParsed)) {
      $message = [
        'No json config can be found.',
        'Ensure you have a valid icomoon selection.json file located in your theme.',
        'Ensure you have configured the ckmoon module correctly.',
      ];
      $form['no_json'] = [
        '#type' => 'inline_template',
        '#template' => "{{message|nl2br}}",
        '#context' => [
          'message' => implode("\n", $message),
        ],
      ];
      return $form;
    }

    // CSS selector from the form activating this dialog.
    // This will be used for replacing values with the selection.
    $form['input_id'] = [
      '#type' => 'hidden',
      '#value' => $input_id,
    ];

    $form['#attached']['library'][] = 'ckmoon/ckmoon';

    $form['icons'] = [
      '#type' => 'fieldset',
      '#attributes' => [
        'class' => [
          'ckmoon__icon-fieldset',
        ],
      ],
    ];

    foreach ($jsonParsed as $icon_code => $icon) {
      $form['icons'][$icon_code] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'ckmoon__icon-container',
          ],
        ],
        'preview' => ['#markup' => $icon],
        'button' => [
          '#type' => 'button',
          '#value' => $icon_code,
          '#attributes' => [
            'class' => [
              $icon_code,
              'ckmoon__icon-button',
            ],
          ],
          '#submit' => [],
          '#ajax' => [
            'callback' => '::submitForm',
            'event' => 'click',
          ],
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $id = $form_state->getValue('input_id');
    $icon_code = $form_state->getTriggeringElement()['#value'];

    // Render the svg markup of the selected icon into a string.
    // This is to be passed onto javascript for jquery to
    // insert into the dom.
    $preview = $form['icons'][$icon_code]['preview'];
    $svg = $this->renderer->render($preview);

    $response->addCommand(new CkmoonSelection('#' . $id, $svg, $icon_code));
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

}

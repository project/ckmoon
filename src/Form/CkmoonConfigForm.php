<?php

namespace Drupal\ckmoon\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for storing CkMoon icons settings.
 */
class CkmoonConfigForm extends ConfigFormBase {

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Defines a CkMoon Icon manager class.
   *
   * @var \Drupal\ckmoon\CkmoonManager
   */
  private $ckmoonManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->ckmoonManager = $container->get('ckmoon.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ckmoon.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ckmoon_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('ckmoon.config');

    $form['#attached']['library'][] = 'ckmoon/admin_form';

    $form['json_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Icomoon Config JSON Path'),
      '#description' => $this->t('Path to the icomoon selection.json file, relative to the default theme.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('json_path'),
    ];
    $form['class_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Icon class prefix'),
      '#description' => $this->t('A string the icons class names are prefixed with. eg: icon--'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('class_prefix'),
    ];

    $options = $this->ckmoonManager->getIconList();

    if (count($options) > 0) {

      $form['icon_exclusion_detail'] = [
        '#type' => 'details',
        '#title' => $this->t('Exclude Icons'),
        '#attributes' => [
          'class' => ['icon-exclude-checkboxes'],
        ],
        '#access' => count($options)
      ];

      $form['icon_exclusion_detail']['icon_exclusion'] = [
        '#type' => 'checkboxes',
        '#multiple' => TRUE,
        '#options' => $options,
        '#default_value' => $config->get('icon_exclusion') ?: [],
      ];
    }

    $form['menu_icon'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable menu icons.'),
      '#description' => $this->t('Check this box to add a icon select field to menu link edit forms.'),
      '#default_value' => $config->get('menu_icon'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('ckmoon.config')
      ->set('json_path', $form_state->getValue('json_path'))
      ->set('class_prefix', $form_state->getValue('class_prefix'))
      ->set('menu_icon', $form_state->getValue('menu_icon'))
      ->set('icon_exclusion', $form_state->getValue('icon_exclusion'))
      ->save();
  }

}

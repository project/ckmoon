<?php

namespace Drupal\ckmoon;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Template\Attribute;
use Drupal\menu_link_content\Entity\MenuLinkContent;

/**
 * Defines a CkMoon Icon manager class.
 */
class CkmoonManager {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The icon class prefix stored in config.
   *
   * @var string
   */
  public $iconClassPrefix;

  /**
   * Ckmoon config from settings form.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  public $ckmoonConfig;

  /**
   * The current theme.
   *
   * @var string
   */
  public $theme;

  /**
   * Path the the current theme.
   *
   * @var string
   */
  public $themePath;

  /**
   * Icomoon selection.json contents.
   *
   * @var object
   */
  public $json;

  /**
   * Icomoon selection.json contents parsed.
   *
   * @var array
   */
  public $jsonParsed;

  /**
   * Turns a render array into an HTML string.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  private Renderer $renderer;

  /**
   * Constructs a new CkmoonManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Defines the interface for a configuration object factory.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   Defines the Renderer service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Renderer $renderer) {
    $this->configFactory = $config_factory;
    $this->ckmoonConfig = $config_factory->get('ckmoon.config');
    $this->renderer = $renderer;
  }

  /**
   * Gets the current theme.
   *
   * @return string|null
   *   The current theme.
   */
  public function getTheme() {
    if (!$this->theme) {
      $this->theme = $this->configFactory->get('system.theme')->get('default');
    }
    return $this->theme;
  }

  /**
   * Gets the path to the current theme.
   *
   * @return string
   *   Path to the current theme.
   */
  public function getThemePath() {
    if (!$this->themePath) {
      // @todo inject through dependency injection.
      // phpcs:ignore
      $this->themePath = \Drupal::service('extension.list.theme')->getPath($this->getTheme());
    }
    return $this->themePath;
  }

  /**
   * Gets the icmoon selection.json contents from the current theme.
   *
   * @return object|null
   *   The icomoon selection.json file.
   */
  public function getIcomoonJson() {
    if (!$this->json) {
      $theme_path = $this->getThemePath();
      $jsonPath = $this->ckmoonConfig->get('json_path');
      if (is_file($theme_path . '/' . $jsonPath)) {
        $this->json = json_decode(file_get_contents($theme_path . '/' . $jsonPath));
      }
    }
    return $this->json;
  }

  /**
   * Gets the parsed icomoon selection.json contents from the current theme.
   *
   * @return array|null
   *   The icomoon selection.json file parsed.
   */
  public function getIcomoonJsonParsed() {
    if (!$this->jsonParsed) {
      if ($json = $this->getIcomoonJson()) {
        $parsed = array_reduce($json->icons, function ($acc, $item) {
          $acc[$item->properties->name] = $item;
          return $acc;
        }, []);
        $this->jsonParsed = $parsed;
      }
    }
    return $this->jsonParsed;
  }

  /**
   * Gets an array of icons keyed by icon_code.
   *
   * @param bool $filtered
   *   Remove excluded icons.
   *
   * @return array
   *   Array of icons.
   *
   * @throws \Exception
   */
  public function getIconList(bool $filtered = FALSE) {
    $array = [];

    if ($json = $this->getIcomoonJsonParsed()) {
      if ($filtered) {

        $excluded_icons = $this->ckmoonConfig->get('icon_exclusion');
        if ($excluded_icons) {
          $exclusions = array_filter($excluded_icons);
          $json = array_filter($json, function ($v) use ($exclusions) {
            return !in_array($v, $exclusions);
          }, ARRAY_FILTER_USE_KEY);
        }
      }

      foreach ($json as $icon_code => $data) {
        $svg = $this->generateIconSvg($icon_code);
        $array[$icon_code] = $this->renderer->render($svg);
      }
    }

    return $array;

  }

  /**
   * Returns an array of theme icons to use in a select input.
   *
   * @return array
   *   An array of them icons.
   */
  public function getIconOptions() {
    $json = $this->getIcomoonJson();

    $options = [];
    foreach ($json->icons as $icon) {
      $name = $icon->properties->name;
      $options[$name] = $name;
    }
    ksort($options);

    return $options;
  }

  /**
   * Returns the icon class prefix stored in config.
   *
   * @return string|null
   *   The icon class prefix.
   */
  public function getIconClassPrefix() {
    if (is_null($this->iconClassPrefix)) {
      $this->iconClassPrefix = $this->ckmoonConfig->get('class_prefix');
    }
    return $this->iconClassPrefix;
  }

  /**
   * Gets the icon associated with a menu link.
   *
   * @param \Drupal\menu_link_content\Entity\MenuLinkContent $menu_link
   *   Menu link item.
   *
   * @return string
   *   Icon code or nothing.
   */
  public function getMenuLinkIconValue(MenuLinkContent $menu_link) {
    $menu_link_options = $menu_link->link ? $menu_link->link->first()->options : [];
    return !empty($menu_link_options['attributes']['data-icon']) ? $menu_link_options['attributes']['data-icon'] : '';
  }

  /**
   * Generates ckmoon_svg markup from icmoon json file.
   *
   * @param string $icon_code
   *   Icomoon icon code.
   *
   * @return array
   *   A renderable ckmoon_svg array.
   */
  public function generateIconSvg($icon_code) {
    $build = [];

    if ($jsonParsed = $this->getIcomoonJsonParsed()) {
      $icon_settings = $jsonParsed[$icon_code] ?? FALSE;

      if (!$icon_settings) {
        return $build;
      }

      $width = $icon_settings->icon->width ?? 1024;
      $height = $icon_settings->icon->height ?? 1024;
      $prevSize = $icon_settings->properties->prevSize ?? FALSE;

      $svg_attributes = new Attribute([
        'xmlns'=> "http://www.w3.org/2000/svg",
        'class' => ['ckmoon__icon'],
        'width' => $prevSize ?: $width,
        'height' => $prevSize ?: $height,
        'viewbox' => "0 0 {$width} {$height}",
      ]);

      $paths = [];
      foreach ($icon_settings->icon->paths as $path) {
        $paths[] = [
          'attributes' => new Attribute(['d' => $path]),
        ];
      }

      $build = [
        '#theme' => 'ckmoon_svg',
        '#svg_attributes' => $svg_attributes,
        '#paths' => $paths,
      ];
    }

    return $build;
  }

  /**
   * Checks if the menu icon functionality is enabled in the config page.
   *
   * @return bool
   *   True if enabled.
   */
  public function menuIconEnabled() {
    return $this->ckmoonConfig->get('menu_icon');
  }

  /**
   * Adds icon css class to menu items via hook_preprocess_menu_items.
   *
   * @param array $items
   *   An array of menu items.
   */
  public function preprocessMenuItems(array &$items) {
    $classPrefix = $this->getIconClassPrefix();
    foreach ($items as $item) {
      /** @var \Drupal\Core\Menu\MenuLinkDefault $menu_link */
      $options = !empty($item['url']) ? $item['url']->getOptions() : NULL;

      if ($options && isset($options['attributes'])) {
        $attributes = $options['attributes'];

        if (isset($attributes['data-icon'])) {
          $icon = $attributes['data-icon'];
          $attributes['class'][] = $classPrefix . $icon;
          $item['url']->setOption('attributes', $attributes);
        }
      }
      if (!empty($item['below'])) {
        $this->preprocessMenuItems($item['below']);
      }
    }
  }

}

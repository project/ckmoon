import { Plugin } from 'ckeditor5/src/core';
import { Widget, toWidget } from 'ckeditor5/src/widget';
import InsertIconCommand from "./command";
import {createElement} from "ckeditor5/src/utils";

/**
 * CKEditor 5 plugins do not work directly with the DOM. They are defined as
 * plugin-specific data models that are then converted to markup that
 * is inserted in the DOM.
 *
 * CKEditor 5 internally interacts with icon as this model:
 * <icon></icon>
 *
 * Which is converted for the browser/user as this markup
 * <span class="ckmoon-class-prefix-*"></span>
 *
 * This file has the logic for defining the icon model, and for how it is
 * converted to standard DOM markup.
 */
export default class Editing extends Plugin {
  static get requires() {
    return [Widget];
  }

  init() {
    this.config = this.editor.config.get('ckmoon');
    this._defineSchema();
    this._defineConverters();
    this.editor.commands.add('insertIcon', new InsertIconCommand(this.editor));
  }

  /*
   * This registers the structure that will be seen by CKEditor 5 as
   * <icon></icon>
   *
   * The logic in _defineConverters() will determine how this is converted to
   * markup.
   */
  _defineSchema() {
    const schema = this.editor.model.schema;

    schema.extend( '$text', { allowAttributes: 'icon' } );

    schema.register('icon', {
      allowAttributes: 'class',
      isObject: true,
      allowWhere: '$text',
      isInline: true,
    });
  }

  /*
   * This logic will determine how <icon> is converted to markup and vice versa.
   */
  _defineConverters() {
    const { conversion } = this.editor;
    const classFindPattern = new RegExp(this.config.classPrefix + '.+');
    const classReplacePattern = new RegExp(this.config.classPrefix);

    // Upcast Converters: determine how existing HTML is interpreted by the
    // editor. These trigger when an editor instance loads.
    //
    // If <span class="ckmoon-class-prefix-*"> is present in the existing markup
    // processed by CKEditor, then CKEditor recognizes and loads it as a
    // <icon> model.
    conversion.for('upcast')
      .elementToElement({
        view: {
          name: 'span',
          classes: classFindPattern,
        },
        model: ( viewElement, { writer } ) => {
          return writer.createElement( 'icon', { class: viewElement.getAttribute( 'class' ) } );
        }
      });

    // Data Downcast Converters: converts stored model data into HTML.
    // These trigger when content is saved.
    //
    // Instances of <icon> are saved as
    // <span class="ckmoon-class-prefix-*"></span>.
    conversion.for('dataDowncast')
      .elementToElement({
        model: 'icon',
        view: (modelElement, { writer: viewWriter }) => {
          return viewWriter.createEmptyElement('span', {
            class:  modelElement.getAttribute('class'),
          });
        }
      });

    // Editing Downcast Converters. These render the content to the user for
    // editing, i.e. this determines what gets seen in the editor. These trigger
    // after the Data Upcast Converters, and are re-triggered any time there
    // are changes to any of the models' properties.
    //
    // Convert the <icon> model into an actual svg element in the editor UI
    conversion.for('editingDowncast')
      .elementToElement({
        model: 'icon',
        view: (modelElement, { writer: viewWriter }) => {

          // Create the widget element
          const widgetElement = viewWriter.createContainerElement( 'span', {
            class: 'ckmoon-icon-widget'
          } );

          // Get the selected icon
          let classname = modelElement.getAttribute('class');
          let iconCode = classname.replace(classReplacePattern, '');
          const icons = this.config.icons
          const icon = icons[iconCode];

          // create the svg element
          const svgUIElement = viewWriter.createUIElement( 'span', {}, function( domDocument ) {
            const domElement = this.toDomElement( domDocument );
            domElement.innerHTML = icon.trim();
            return domElement;
          });

          viewWriter.insert( viewWriter.createPositionAt( widgetElement, 0 ), svgUIElement );

          return toWidget( widgetElement, viewWriter, { label: iconCode } );
        }
      })
  }
}

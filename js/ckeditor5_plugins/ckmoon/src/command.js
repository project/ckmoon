import { Command } from 'ckeditor5/src/core';

export default class InsertIconCommand extends Command {
  execute( classname ) {
    const { model } = this.editor;
    this.config = this.editor.config.get('ckmoon');

    model.change(writer => {
      model.insertContent(this.createIcon(writer, classname));
    });
  }

  refresh() {
    const { model } = this.editor;
    const { selection } = model.document;

    // Determine if the cursor (selection) is in a position where adding an
    // icon is permitted. This is based on the schema of the model(s)
    // currently containing the cursor.
    const allowedIn = model.schema.findAllowedParent(
      selection.getFirstPosition(),
      'icon',
    );

    // If the cursor is not in a location where an icon can be added, return
    // null so the addition doesn't happen.
    // this.isEnabled = allowedIn !== null;
    this.isEnabled = allowedIn !== null;
  }

  createIcon(writer, value) {
    const icon = writer.createElement('icon', { class: this.config.classPrefix + value });
    writer.setAttributes({ class: this.config.classPrefix + value }, icon);
    writer.insertText('', icon);
    return icon;
  }
}

// function createIcon(writer, value) {
//   const icon = writer.createElement('icon', { class: this.config.classPrefix + value });
//   writer.setAttributes({ class: this.config.classPrefix + value }, icon);
//   writer.insertText('', icon);
//   return icon;
// }
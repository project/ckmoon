<?php

namespace Drupal\ckmoon\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Ajax\EditorDialogSave;

/**
 * Provides a CKMoon Dialog form for ckeditor plugin.
 */
class CkmoonEditorDialog extends CkMoonDialog {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ckmoon_editor_dialog';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new EditorDialogSave($form_state->getTriggeringElement()['#value']));
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

}

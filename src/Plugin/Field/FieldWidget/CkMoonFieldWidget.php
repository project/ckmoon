<?php

namespace Drupal\ckmoon\Plugin\Field\FieldWidget;

use Drupal\ckmoon\CkmoonManager;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'ckmoon' widget.
 *
 * @FieldWidget(
 *   id = "ckmoon",
 *   module = "ckmoon",
 *   label = @Translation("CkMoon Select"),
 *   field_types = {
 *     "ckmoon"
 *   }
 * )
 */
class CkMoonFieldWidget extends WidgetBase {

  /**
   * Defines a CKmoon Icon manager class..
   *
   * @var \Drupal\ckmoon\CkmoonManager
   */
  protected $ckmoonManager;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\ckmoon\CkmoonManager $ckmoon_manager
   *   Defines a CKmoon Icon manager class.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, CkmoonManager $ckmoon_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->ckmoonManager = $ckmoon_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('ckmoon.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $form['#attached']['library'][] = 'ckmoon/dialog.ajax';

    $field_name = $this->fieldDefinition->getName();
    $id = Html::getUniqueId('ckmoon_value');
    $remove_button_state_selector = "{$field_name}[{$delta}][ckmoon][ckmoon_value]";

    $saved_value = $items[$delta]->value ?? NULL;
    $element_parents = [$field_name, $delta, 'ckmoon', 'ckmoon_value'];
    $selected_value = NestedArray::getValue($form_state->getUserInput(), $element_parents);
    $value = $selected_value ?: $saved_value;
    $svg = $this->ckmoonManager->generateIconSvg($value);

    // @todo create a custom form element for this.
    $element['ckmoon'] = [
      '#type' => 'details',
      '#title' => 'Ckmoon Icon',
      '#attributes' => [
        'class' => ['ckmoon-select-element'],
      ],
      '#prefix' => '<div id="' . $id . '">',
      '#suffix' => '</div>',
      'button' => [
        '#type' => 'link',
        '#title' => $this->t('Select an icon'),
        '#url' => Url::fromRoute('ckmoon.ckmoon_dialog', ['input_id' => $id]),
        '#attributes' => [
          'class' => ['button', 'use-ajax'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => json_encode(['width' => 900]),
        ],
        '#weight' => 0,
      ],
      'remove' => [
        '#type' => 'button',
        '#value' => $this->t('Remove icon'),
        '#name' => 'remove_' . $delta,
        '#submit' => [[get_class($this), 'removeItemSubmit']],
        '#attributes' => [
          'class' => ['remove-button'],
        ],
        '#states' => [
          'visible' => [
            'input[name="' . $remove_button_state_selector . '"]' => ['filled' => TRUE],
          ],
        ],
        '#ajax' => [
          'callback' => [get_class($this), 'removeItemSubmit'],
          'wrapper' => $id,
          'event' => 'click',
        ],
      ],
      'ckmoon_preview' => [
        '#type' => 'container',
        '#attributes' => ['class' => ['ckmoon-preview']],
        'svg' => $svg,
        '#weight' => 1,
      ],
      'ckmoon_preview_title' => [
        '#type' => 'container',
        '#attributes' => ['class' => ['ckmoon-preview-title']],
        'selected' => [
          '#markup' => $value,
        ],
        '#weight' => 2,
      ],
      'ckmoon_value' => [
        '#type' => 'textfield',
        '#attributes' => [
          'class' => [
            'ckmoon-value',
            'hidden',
          ],
        ],
        '#default_value' => $value,
        '#weight' => 3,
      ],
    ];

    return $element;
  }

  /**
   * Submit callback for remove buttons.
   */
  public static function removeItemSubmit(&$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $parents = array_slice($trigger['#parents'], 0, -1);
    $form_parents = array_slice($trigger['#array_parents'], 0, -1);
    $widget = NestedArray::getValue($form, $form_parents);
    $target_id_element = &NestedArray::getValue($form, array_merge($form_parents, ['ckmoon_value']));

    $form_state->setValueForElement($target_id_element, '');
    NestedArray::setValue($form_state->getUserInput(), array_merge($parents, ['ckmoon_value']), '');
    $widget['ckmoon_value']['#value'] = NULL;
    $widget['ckmoon_preview']['svg'] = NULL;
    $widget['ckmoon_preview_title']['selected']['#markup'] = NULL;

    $form_state->setRebuild();

    return $widget;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      if (!empty($value['ckmoon']['ckmoon_value'])) {
        $value['value'] = $value['ckmoon']['ckmoon_value'];
      }
    }
    return $values;
  }

}

import { Plugin } from 'ckeditor5/src/core';
import Ui from "./ui";
import Editing from "./editing";
export default class Ckmoon extends Plugin {

  static get requires() {
    return [Editing, Ui];
  }

}

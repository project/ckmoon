/**
 * @file registers the ckmoon toolbar button and binds functionality to it.
 */

import { Plugin } from 'ckeditor5/src/core';
import { Collection } from 'ckeditor5/src/utils.js';
import { addListToDropdown, createDropdown, Model } from 'ckeditor5/src/ui';
import icon from '../../../../icons/ckmoon.svg';

export default class Ui extends Plugin {
  init() {
    const editor = this.editor;
    const config = editor.config.get('ckmoon');

    // This will register the icon toolbar button.
    editor.ui.componentFactory.add('Ckmoon', (locale) => {

      // Create collection to store icons.
      const itemDefinitions = new Collection();

      // Loop through each icon from the config and create a new itemDefinition.
      for (const [key, value] of Object.entries(config.icons)) {

        let def = {
          type: 'button',
          model: new Model( {
            icon: value,
            tooltip: key
          })
        }

        def.model.set( {
          commandName: 'insertIcon',
          commandValue: key
        } );

        itemDefinitions.add( def );
      }

      // Create the Dropdown button for ckmoon that sits in the wysiwyg toolbar.
      const dropdownView = createDropdown( locale );
      // Add our icon definitions to the dropdown.
      addListToDropdown( dropdownView, itemDefinitions );
      // Add a class to the dropdowns icon panel for styling.
      dropdownView.panelView.template.attributes.class.push('ckmoon-ck-panel');

      // Define the dropdowns button properties.
      dropdownView.buttonView.set( {
        label: editor.t('Icon'),
        icon,
        tooltip: true,
        class: 'ckmoon-ck-button'
      } );

      // Listen to the execute event being called when an icon is clicked.
      this.listenTo( dropdownView, 'execute', evt => {
        const { commandName, commandValue } = evt.source;
        editor.execute( commandName, commandValue );
        editor.editing.view.focus();
      } );

      return dropdownView;
    });
  }
}
